#!/bin/bash
ganadores=list()

for i in range(5):
    num=int(input("ingrese el " + str(i+1) + " numero: "))

    while (num not in range(1,49)):
        print("ingrese un numero valido (1 al 49)")
        num=int(input("ingrese el " + str(i+1) + " numero: "))

    ganadores.append(num)    

print(sorted(ganadores))
