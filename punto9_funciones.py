#!/bin/Python3
import random

def ingresar_numero():
    i= int(input("Ingrese un numero entre 1 y 100: "))

    while i not in range (1,101):
        i=int(input("El numero ingresado no esta entre 1 y 100, ingrese otro: "))
    return i
   
def generar_random():
    return random.randint(1,101)

def es_correcto(num,numGen):
    if num == numGen:
        print("Ha Ganado!")
    elif(num > numGen):
        print("El numero " + str(num) + "es mayor que el numero generado " + str(numGen))
    else:
        print("El numero " + str(num) + "es menor que el numero generado " + str(numGen))
