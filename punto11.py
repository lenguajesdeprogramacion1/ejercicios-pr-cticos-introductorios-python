#!/bin/Python3
import datetime

    

nacimiento = input("Ingrese su fecha de nacimiento (Dia/Mes/Año): ")
nac_date = datetime.datetime.strptime(nacimiento, '%d/%m/%Y')

dif = datetime.datetime.now()  - nac_date

segundos = dif.seconds
horas, segundos = divmod(segundos, 3600)
minutos, segundos = divmod(segundos, 60)

print("Han pasado " + str(dif.days) + " dias " + str(horas) + " horas " + str(minutos) + " minutos y " + str(segundos) + " segundos desde su nacimiento") 
